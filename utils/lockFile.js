const fs = require('fs');
const path = require('path');
const yaml = require('yaml');

const fileLocation = path.join(__dirname, '..', 'pnpm-lock.yaml');

function read() {
  const content = fs.readFileSync(fileLocation, 'utf8').toString();

  return yaml.parse(content);
}

function write(json) {
  fs.writeFileSync(fileLocation, yaml.stringify(json));
}

module.exports = {
  read,
  write,
}