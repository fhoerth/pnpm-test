const getTagName = require('./getTagName');

function getTarballName(pkgPackageJson) {
  return getTagName(pkgPackageJson).concat('.tgz');
}

module.exports = getTarballName;