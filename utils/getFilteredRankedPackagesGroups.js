const getRankedPackagesGroups = require('./getRankedPackagesGroups');

const rankedPackagesGroups = getRankedPackagesGroups();

function getFilteredRankedPackagesGroups(rootPath) {
  if (!rootPath) {
    return rankedPackagesGroups;
  }

  return rankedPackagesGroups.map(group => {
    const newGroup = group
      .filter(rankedPackage => {
        return rankedPackage.relativePath.includes(rootPath) || rootPath.includes(rankedPackage.relativePath);
      });
    
    return newGroup.length ? newGroup : null;
  }).filter(Boolean);
}

module.exports = getFilteredRankedPackagesGroups;