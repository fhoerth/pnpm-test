const getPackages = require('./getPackages');

const packages = getPackages();

const findPackageByName = (packageName) => packages.find(x => x.name === packageName);

module.exports = findPackageByName;