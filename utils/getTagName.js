function getTagName(pkgPackageJson) {
  return pkgPackageJson
    .name
    .toLowerCase()
    .replace('@', '')
    .replace('/', '-')
    .concat('-')
    .concat(pkgPackageJson.version);
}

module.exports = getTagName;