const findPackageByName = require('./findPackageByName');

const getInternalPackages = (dependencies) => {
  if (!dependencies) {
    return [];
  }

  return Object.keys(dependencies)
    .filter(packageName => findPackageByName(packageName))
    .map(packageName => findPackageByName(packageName));
};

const getInternalDependencies = (packageJson) => {
  const { devDependencies, dependencies, peerDependencies } = packageJson;

  return {
    dependencies: getInternalPackages(dependencies),
    devDependencies: getInternalPackages(devDependencies),
    peerDependencies: getInternalPackages(peerDependencies),
  };
};

module.exports = getInternalDependencies;
