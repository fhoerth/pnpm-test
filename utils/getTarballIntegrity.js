const fs = require('fs');
const crypto = require('crypto');

const getTarballIntegrity = (tarballPath) => {
  const content = fs.readFileSync(tarballPath);
  const algorithm = 'sha512';
  
  const shasum = crypto.createHash(algorithm);
  shasum.update(content);
  const result = algorithm.concat('-').concat(shasum.digest('base64'));

  return result;
};

module.exports = getTarballIntegrity;