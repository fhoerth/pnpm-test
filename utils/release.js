const execa = require('execa');
const path = require('path');
const rimraf = require('rimraf');
const chalk = require('chalk');
const fs = require('fs');
const copydir = require('copy-dir');
const findPackageByName = require('./findPackageByName');
const git = require('./git');
const getTagName = require('./getTagName');

const packageName = process.argv[2];
const package = findPackageByName(packageName)

if (!package) {
  process.stdout.write(chalk.rgb(255,0 ,0 ).bold(`\u2718 The package \`${packageName}\` was not found.`));
  process.stdout.write('\r\n');
  process.exit(1);
}

const currentLocalBranch = git.getCurrentLocalBranch();
const tagName = getTagName(package['package.json']);
const gitBranchName = 'build/'.concat(tagName);
const gitTagName = 'release/'.concat(tagName);

git.ensureCWDIsClean();
git.removeLocalBranch(gitBranchName);
git.removeLocalTag(gitTagName);
git.removeRemoteBranch(gitBranchName);
git.removeRemoteTag(gitTagName);
git.createBranch(gitBranchName);

const tagContentDirectory = 'tag-content';
const rootPath = path.join(__dirname, '..');

const keepDirectories = ['tarballs', '.git', tagContentDirectory];
const keepFiles = ['pnpm-lock.yaml'];

rimraf.sync(path.join(rootPath, tagContentDirectory));
fs.mkdirSync(path.join(rootPath, tagContentDirectory));

copydir.sync(path.join(rootPath, package.relativePath), path.join(rootPath, tagContentDirectory))

rimraf.sync(path.join(rootPath, tagContentDirectory, 'node_modules'));

const scan = fs.readdirSync(rootPath);

scan.forEach(fileOrDir => {
  const fileOrDirPath = path.join(rootPath, fileOrDir);
  const stats = fs.statSync(fileOrDirPath);

  if (stats.isFile() && keepFiles.includes(fileOrDir)) {
    fs.copyFileSync(path.join(rootPath, fileOrDir), path.join(rootPath, tagContentDirectory, fileOrDir));
  }

  if (!fileOrDir.includes(tagContentDirectory) && stats.isDirectory() && keepDirectories.includes(fileOrDir)) {
    copydir.sync(path.join(rootPath, fileOrDir), path.join(rootPath, tagContentDirectory, fileOrDir));
  }
});

copydir.sync(path.join(rootPath, 'utils'), path.join(rootPath, tagContentDirectory, 'utils'))
execa.sync('node', [path.join(rootPath, tagContentDirectory, 'utils', 'buildLockFile.js')]);
rimraf.sync(path.join(rootPath, tagContentDirectory, 'utils'));

scan.forEach(fileOrDir => {
  if (!fileOrDir.includes(tagContentDirectory)) {
    const fileOrDirPath = path.join(rootPath, fileOrDir);
    const stats = fs.statSync(fileOrDirPath);
  
    if (stats.isDirectory()) {
      rimraf.sync(fileOrDirPath);
    }
    
    if (stats.isFile()) {
      fs.unlinkSync(fileOrDirPath);
    }
  }
});

copydir.sync(path.join(rootPath, tagContentDirectory), rootPath);
rimraf.sync(path.join(rootPath, tagContentDirectory));

git.commit(`Release ${tagName}.`);
git.push();

git.createAndPushTag(gitTagName);

git.checkout(currentLocalBranch);

git.removeLocalBranch(gitBranchName);
git.removeRemoteBranch(gitBranchName);
git.removeRemoteBranch(gitTagName);
git.fetch(gitTagName);

process.stdout.write(chalk.green.bold(`\u2713 The tag "${gitTagName}" was created successfuly.`));
process.stdout.write('\r\n');