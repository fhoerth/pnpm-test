const chalk = require('chalk');

const colorWheel = ['cyan', 'magenta', 'blue', 'yellow', 'green', 'red'];

function colors() {
  let colorIdx = -1;

  return {
    next: () => {
      if (colorIdx === colorWheel.length - 1) {
        colorIdx = -1;
      }

      colorIdx += 1;

      return chalk[colorWheel[colorIdx]];
    }
  };
}

module.exports = colors;