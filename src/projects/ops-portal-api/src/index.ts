import express from 'express';
import package2 from '@ipsy/package-2';

const app = express();

app.get('/', (_req: express.Request, res: express.Response) => res.json(package2()));

app.listen(4873, () => {
  console.log('\n\n Listening on http://localhost:4873/ \n\n');
});