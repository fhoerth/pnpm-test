// This file is run by release.js script.
// The package.json file refers to the package being deployed.
const lockFile = require('./lockFile');
const packageJSON = require('../package.json');
const packages = require('../tarballs/packages.json');

function buildLockFile() {
  const lockfile = lockFile.read();

  const importers = lockfile.importers;

  const packagesByName = packages.reduce((acc, package) => ({
    ...acc,
    [package.name]: package,
  }), {});

  const transformDependencies = (dependencies) => {
    const keys = Object.keys(dependencies);
  
    return keys.reduce((acc, dependencyKey) => {
      if (packagesByName[dependencyKey]) {
        return { ...acc, [dependencyKey]: packagesByName[dependencyKey]['package.json'].version };
      }
  
      return { ...acc, [dependencyKey]: dependencies[dependencyKey] };
    }, {})
  };
  
  const transformImporter = (package, dev) => {
    const importer = importers[package.relativePath];
    const key = '/'.concat(package.name).concat('/').concat('0.0.0');
    const name = package.name;
    // const version = package['package.json'].version;
    // Workaround to registry pnpm error.
    const registry = '';
    const resolution = {
      registry,
      integrity: package.tarballIntegrity,
      tarball: 'file:'.concat(package.tarballRelativePath),
    };
    const dependencies = importer.dependencies ? transformDependencies(importer.dependencies) : null;
    const devDependencies = importer.devDependencies ? transformDependencies(importer.devDependencies) : null;
  
    return {
      [key]: {
        dev,
        registry,
        name,
        resolution,
        // version,
        ...(dependencies ? { dependencies } : null),
        ...(devDependencies ? { devDependencies } : null),
      }
    }
  };
  
  const package = packagesByName[packageJSON.name];
  
  const lockPackages = [
    ...package.dependencyTree.dependencies.map((dependencyPkg) => {
      return transformImporter(packagesByName[dependencyPkg.name], false);
    }),
    ...package.dependencyTree.devDependencies.map((devDependencyPkg) => {
      return transformImporter(packagesByName[devDependencyPkg.name], true);
    }),
  ].reduce((acc, lockPackage) => {
    return { ...acc, ...lockPackage };
  }, {});
  
  lockfile.packages = { ...lockfile.packages, ...lockPackages };
  lockfile.dependencies = transformDependencies(importers[package.relativePath].dependencies);
  lockfile.devDependencies = transformDependencies(importers[package.relativePath].devDependencies);
  lockfile.specifiers = lockfile.importers[package.relativePath].specifiers;

  delete lockfile.importers;
  

  lockFile.write(lockfile);
}

buildLockFile();
