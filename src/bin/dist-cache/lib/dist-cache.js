const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const execa = require('execa');
const copydir = require('copy-dir');

const cacheDir = path.join(__dirname, '..', '.cache');

if (!fs.existsSync(cacheDir)) {
  fs.mkdirSync(cacheDir);
}

const commandIdx = process.argv.indexOf('-x');
const [executable, ...executableArgs] = process.argv[commandIdx + 1].split(' ');

const outDirs = process.argv.reduce((acc, arg, idx) => {
  if (arg === '--outDir') {
    return acc.concat(process.argv[idx + 1]);
  }

  return acc;
}, []);

const ignore = process.argv.reduce((acc, arg, idx) => {
  if (arg === '--ignore') {
    return acc.concat(process.argv[idx + 1]);
  }

  return acc;
}, []).concat('node_modules');


const algorithm = 'sha1';
const getHash = (content) => {
  const shasum = crypto.createHash(algorithm);
  shasum.update(content);

  return shasum.digest('hex');
};

const dirStructure = (dir = process.cwd()) => {
  const files = [];
  const dirs = [];

  (
    dir === process.cwd()
      ? fs.readdirSync(dir).filter(x => !ignore.includes(x) && !outDirs.includes(x))
      : fs.readdirSync(dir)
  ).forEach(fileOrDir => {
    const stats = fs.statSync(path.join(dir, fileOrDir));

    if (stats.isDirectory()) {
      dirs.push(fileOrDir);
    }

    if (stats.isFile()) {
      files.push(fileOrDir);
    }
  });

  return {
    [dir]: {
      files: files.reduce((acc, file) => {
        const fileContent = fs.readFileSync(path.join(dir, file), 'utf8').toString();
        const fileHash = getHash(fileContent);
        return {
          ...acc,
          [file]: fileHash,
        }
      }, {}),
      dirs: dirs.reduce((acc, subdir) => {
        return {
          ...acc,
          [subdir]: dirStructure(path.join(dir, subdir)),
        }
      }, {}),
    },
  }
};

const dirStructureHash = () => {
  const content = JSON.stringify(dirStructure());

  return getHash(content);
}

const targetDir = path.join(cacheDir, dirStructureHash());

if (fs.existsSync(targetDir)) {
  // Copy cache.
  outDirs.forEach(outDir => {
    copydir.sync(path.join(targetDir, outDir), path.join(process.cwd(), outDir));
  });

  process.exit(0);
} else {
  const child = execa(executable, executableArgs, { stdio: 'inherit' });

  child.on('exit', (exitCode) => {
    // Copy output.
    fs.mkdirSync(targetDir);

    outDirs.forEach(outDir => {
      copydir.sync(path.join(process.cwd(), outDir), path.join(targetDir, outDir));
    })

    process.exit(exitCode);
  });
}