const stripAnsi = require('strip-ansi');
const chalk = require('chalk');

const hasNewLine = (data) => {
  return /\r\n/.test(data);
}

class Buffer {
  constructor({
    packageName, executable, processArgs, color, streaming
  }) {
    this.line = '';
    this.lines = [];
    this.packageName = packageName;
    this.executable = executable;
    this.processArgs = processArgs;
    this.color = color;
    this.isFirstNewLineStripped = false;
    this.streaming = !!streaming;
  }

  clear() {
    for (let i = 0; i < process.stdout.rows; i += 1) {
      this.printNewLine();
    }

    process.stdout.write('\x1bc');
  }

  getBorder() {
    return '⎢ '
  }

  getCommand() {
    return `${this.executable} ${this.processArgs.join(' ')}`;
  }

  getHeader() {
    return `⎡ ${this.packageName} (\`${this.getCommand()}\`)`;
  }

  print(line) {
    process.stdout.write(line);
  }

  printNewLine() {
    this.print('\r\n');
  }

  printHeader() {
    this.printNewLine();
    this.print(this.color.bold(this.getHeader()));
    this.printNewLine();
    this.print(this.color.bold(this.getBorder().concat('='.repeat(this.getHeader().length - this.getBorder().length))));
    this.printNewLine();
  }

  concat(_data) {
    let data = _data;
    
    if (!this.isFirstNewLineStripped) {
      data = data.replace(/^\r\n/, '');

      this.isFirstNewLineStripped = true;
    }

    if (this.beginWithNewLine) {
      data = '\r\n'.concat(data);
    }

    if (!!stripAnsi(data)) {
      if(hasNewLine(data)) {
        const [head, ...tail] = data.split('\r\n');
        
        this.line += head;

        if (this.streaming) {
          this.flush();
        } else {
          this.lines = this.lines.concat(this.line);
        }

        this.line = '';

        this.concat((tail || []).join('\r\n'));
      } else {
        this.line += data;
      }
    }
  }

  flush() {
    const print = line => {
      this.print(this.color.bold(this.getBorder()));
      this.print(line);
      this.printNewLine();
    };

    if (!this.streaming) {
      this.lines.forEach(print);
    } else {
      print(this.line);
    }
  }

  end(error, exitCode) {
    this.print(this.color.bold(this.getBorder()));

    if (error) {
      this.printNewLine();
      this.print(this.color.bold(this.getBorder()));
      this.print(chalk.rgb(255, 0, 0).bold(`Command \`${this.getCommand()}\` execution has failed (exit code ${exitCode}).`));
    } else {
      this.print(chalk.green.bold(`Command \`${this.getCommand()}\` has been executed successfuly.`));
    }
    this.printNewLine();
    this.printNewLine();
  }
}

module.exports = Buffer;