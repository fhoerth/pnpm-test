"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const package_2_1 = __importDefault(require("@ipsy/package-2"));
const app = express_1.default();
app.get('/', (_req, res) => res.json(package_2_1.default()));
app.listen(4873, () => {
    console.log('\n\n Listening on http://localhost:4873/ \n\n');
});
