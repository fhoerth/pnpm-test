const path = require('path');
const rimraf = require('rimraf');
const chalk = require('chalk');
const util = require('util');
const fs = require('fs');

const config = require('./config');
const createColors = require('./colors');
const getPackages = require('./getPackages');
const getTarballIntegrity = require('./getTarballIntegrity');
const getInternalDependencies = require('./getInternalDependencies');
const getDependencyTree = require('./getDependencyTree');

const fsExists = util.promisify(fs.exists);
const fsMkdir = util.promisify(fs.mkdir);
const fsCopy = util.promisify(fs.copyFile);
const fsUnlink = util.promisify(fs.unlink);
const fsWriteFile = util.promisify(fs.writeFile);

const packages = getPackages();
const colors = createColors();

const rootPath = path.join(__dirname, '..');

process.stdout.write('\r\n');

const collect = async(packageIndex = 0) => {
  if (typeof packages[packageIndex] === 'undefined') {
    return null;
  }

  const package = packages[packageIndex];

  const originRelativePath = path.join(package.relativePath, package.tarballName);
  const originPath = path.join(rootPath, originRelativePath);
  const destinationRelativePath = package.tarballRelativePath;
  const destinationPath = path.join(rootPath, destinationRelativePath);
  const color = colors.next();

  process.stdout.write(color.bold('- Collecting package '.concat(package.name.concat(` tarball (\`${originRelativePath}\`).`))));
  process.stdout.write('\r\n');

  try {
    const fileExists = await fsExists(originPath);

    if (fileExists) {
      process.stdout.write(chalk.rgb(200, 200, 0)(`\u2713 Tarball \`${originRelativePath}\` exists.`));
      process.stdout.write('\r\n');

      try {
        await fsCopy(originPath, destinationPath);
        process.stdout.write(chalk.green.bold(`\u2713 Tarball \`${originRelativePath}\` copied to \`${destinationRelativePath}\` successfuly.`));

        try {
          await fsUnlink(originPath);
          process.stdout.write('\r\n');
          process.stdout.write(chalk.green.bold(`\u2713 Tarball \`${originRelativePath}\` deleted successfuly.`));
        } catch (unlinkError) {
          process.stdout.write('\r\n');
          process.stdout.write(chalk.rgb(255, 0,0 ).bold(`\u2718 Tarball \`${originRelativePath}\` couldn\'t be deleted.`));
        }

      } catch (copyError) {
        process.stdout.write(chalk.rgb(255, 0,0 ).bold(`\u2718 Tarball \`${originRelativePath}\` couldn\'t be copied to \`${destinationPath}\`.`));
        process.stdout.write('\r\n');
        process.stdout.write('\r\n');
        console.error(copyError);
        process.exit(1);
      }

      process.stdout.write('\r\n');
      process.stdout.write('\r\n');
      return collect(packageIndex + 1);
    } else {
      process.stdout.write(chalk.rgb(255, 0,0 ).bold(`\u2718 Tarball \`${originRelativePath}\` doesn\'t exist.`));
      process.stdout.write('\r\n');
      process.stdout.write('\r\n');
      process.exit(1);
    }
  } catch (readError) {
    process.stdout.write(chalk.rgb(255, 0,0 ).bold(`\u2718 Error while reading tarball \`${originRelativePath}\`.`));
    process.stdout.write('\r\n');
    process.stdout.write('\r\n');
    console.error(readError);
    process.exit(1);
  }
};

const main = async() => {
  const tarballsPath = path.join(rootPath, config.tarballsRelativePath);

  rimraf(tarballsPath, async () => {
    await fsMkdir(tarballsPath);
    await collect(0);

    const packagesWithTarballsIntegrity = packages.map(package => ({
      ...package,
      tarballIntegrity: getTarballIntegrity(path.join(rootPath, package.tarballRelativePath)),
      internalDependencies: getInternalDependencies(package['package.json']),
      dependencyTree: getDependencyTree(package['package.json']),
    }));

    await fsWriteFile(path.join(tarballsPath, 'packages.json'), JSON.stringify(packagesWithTarballsIntegrity, null, 2));
  });
};

main();