const getInternalDependencies = require('./getInternalDependencies');
const findPackageByName = require('./findPackageByName');

const getDifference = (dependencies, devDependencies) => {
  const set = new Set();
  const newDevDependencies = [];

  dependencies.forEach(package => {
    set.add(package.name);
  });

  devDependencies.forEach(package => {
    if (!set.has(package.name)) {
      newDevDependencies.push(package);
    }
  });

  return newDevDependencies;
};

const dedupe = (packages) => {
  const set = new Set();
  const newPackages = [];
  
  packages.forEach(package => {
    if (!set.has(package.name)) {
      set.add(package.name);
      newPackages.push(package);
    }
  });

  return newPackages;
};

function getDependencyTree(pkgPackageJson) {
  // console.log(pkgPackageJson)
  /**
   * This function returns an array of dendencies/devDependencies given for a specific package.
   * A dependency is considered "devDependency" when the dependency is not recursively listed in a package "dependencies",
   * and is considered "dependency" when at least is listed once in a package "dependencies". 
   */
  const { dependencies, devDependencies } = getInternalDependencies(pkgPackageJson);

  const recursiveDependencies = [].concat(
    dependencies.reduce((acc, dependency) => {
      return acc.concat(getDependencyTree(dependency['package.json']).dependencies)
    }, [])
  ).concat(
    devDependencies.reduce((acc, dependency) => {
      return acc.concat(getDependencyTree(dependency['package.json']).dependencies)
    }, [])
  );

  const recursiveDevDependencies = [].concat(
    devDependencies.reduce((acc, devDependency) => {
      return acc.concat(getDependencyTree(devDependency['package.json']).devDependencies)
    }, [])
  ).concat(
    dependencies.reduce((acc, devDependency) => {
      return acc.concat(getDependencyTree(devDependency['package.json']).devDependencies)
    }, [])
  );

  const dedupedDependencies = dedupe([
    ...dependencies,
    ...recursiveDependencies,
  ]);

  const dedupedDevDependencies = dedupe([
    ...devDependencies,
    ...recursiveDevDependencies,
  ]);

  return {
    dependencies: dedupedDependencies,
    devDependencies: getDifference(dedupedDependencies, dedupedDevDependencies),
  };
}
console.log(getDependencyTree(findPackageByName('@ipsy/ops-portal-api')['package.json']));

module.exports = getDependencyTree;
