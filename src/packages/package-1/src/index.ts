import camelcase from 'camelcase';
import { date } from '@ipsy/common';

export default () => ({
  date: date(),
  message: camelcase('package-with-camelcase-1')
});
