const path = require('path');
const getTarballName = require('./getTarballName');
const config = require('./config');

function getTarballRelativePath(pkgPackageJson) {
  return path.join(config.tarballsRelativePath, getTarballName(pkgPackageJson));
}

module.exports = getTarballRelativePath;