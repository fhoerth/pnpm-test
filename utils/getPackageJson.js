const fs = require('fs');
const path = require('path');

function getPackageJson(pkgRelativePath) {
  const packageJson = fs.readFileSync(path.join(__dirname, '..', pkgRelativePath, 'package.json'), 'utf8');
  
  return JSON.parse(packageJson);
}

module.exports = getPackageJson;