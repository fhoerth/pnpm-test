const execa = require('execa');

const argv = process.argv.slice(2, process.argv.length);
const child = execa('tsc', argv, {
  stdio: 'ignore',
});

child.on('exit', (exitCode) => {
  process.exit(exitCode);
});