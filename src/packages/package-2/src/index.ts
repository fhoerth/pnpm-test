import camelcase from 'camelcase';

import package1 from '@ipsy/package-1';
import { date } from '@ipsy/common';

export default () => ({
  package1: package1(),
  package2: {
    date: date(),
    message: camelcase('package-2'),
  },
});