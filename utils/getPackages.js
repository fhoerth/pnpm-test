const path = require('path');

const getPackageJson = require('./getPackageJson');
const getTarballName = require('./getTarballName');
const getTarballRelativePath = require('./getTarballRelativePath');

function getPackages() {
  return [
    {
      name: '@ipsy/dist-cache',
      relativePath: path.join('src', 'bin', 'dist-cache'),
    },
    {
      name: '@ipsy/tsc-silent',
      relativePath: path.join('src', 'bin', 'tsc-silent'),
    },
    {
      name: '@ipsy/ops-portal-api',
      relativePath: path.join('src', 'projects', 'ops-portal-api'),
    },
    {
      name: '@ipsy/common',
      relativePath: path.join('src', 'packages', 'common'),
    },
    {
      name: '@ipsy/package-1',
      relativePath: path.join('src', 'packages', 'package-1'),
    },
    {
      name: '@ipsy/package-2',
      relativePath: path.join('src', 'packages', 'package-2'),
    },
  ].map(x => ({
    ...x,
    'package.json': getPackageJson(x.relativePath),
  })).map(x => ({
    ...x,
    tarballName: getTarballName(x['package.json']),
    tarballRelativePath: getTarballRelativePath(x['package.json']),
  }));
}

module.exports = getPackages;