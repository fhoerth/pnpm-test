// https://gist.github.com/jasonrudolph/1810768
const execa = require('execa');

const getRemoteOriginUrl = () => {
  const executable = 'git';
  const args = ['config', '--get', 'remote.origin.url'];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return result.stdout;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const getCurrentLocalBranch = () => {
  const executable = 'git';
  const args = ['rev-parse', '--abbrev-ref', 'HEAD'];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return result.stdout;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const getLocalLastCommitHash = () => {
  const executable = 'git';
  const args = ['rev-parse', 'HEAD'];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return result.stdout;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const getRemoteLastCommitHash = () => {
  const executable = 'git';
  const args = ['ls-remote', '--exit-code', getRemoteOriginUrl(), getCurrentLocalBranch()];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      const [hash] = result.stdout.split('\t');
      return hash;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const getLastCommitHash = () => {
  return {
    local: getLocalLastCommitHash(),
    remote: getRemoteLastCommitHash(),
  };
}

const getUntrackedFiles = () => {
  const executable = 'git';
  const args = ['ls-files', '.', '--exclude-standard', '--others'];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return result.stdout.split('\n').filter(x => !!x);
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const getChangesToBeCommited = () => {
  const executable = 'git';
  const args = ['diff', '--name-only', '--cached'];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return result.stdout.split('\n').filter(x => !!x);
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const getChangesNotStagedForCommit = () => {
  const executable = 'git';
  const args = ['diff', '--name-only'];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return result.stdout.split('\n').filter(x => !!x);
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const remoteBranchExists = (branchName) => {
  const remoteOriginUrl = getRemoteOriginUrl();

  const executable = 'git';
  const args = ['ls-remote', '--heads', remoteOriginUrl, branchName];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return !!result.stdout;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const remoteTagExists = (tagName) => {
  const remoteOriginUrl = getRemoteOriginUrl();

  const executable = 'git';
  const args = ['ls-remote', '--tags', remoteOriginUrl, tagName];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return !!result.stdout;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const localBranchExists = (branchName) => {
  // This command throws an error when the branch doesn't exist.
  const executable = 'git';
  const args = ['rev-parse', '--verify', branchName];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return true;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    return false;
  }
};

const localTagExists = (branchName) => {
  // This command throws an error (exitCode = 1) when the branch doesn't exist.

  const executable = 'git';
  const args = ['show-ref', '--hash', '--tags', branchName];

  try {
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode === 0) {
      return !!result.stdout;
    } else {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    if (e.exitCode === 1) {
      return false;
    }

    console.error(e);

    process.exit(1);
  }
};

const removeRemoteBranch = (branchName) => {
  const branchExists = remoteBranchExists(branchName);

  if (branchExists) {
    const executable = 'git';
    const args = ['push', 'origin', '--delete', branchName];

    try {
      const result = execa.sync(executable, args, { stdio: 'pipe' });

      if (result.exitCode !== 0) {
        throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
      }
    } catch (e) {
      console.error(e);

      process.exit(1);
    }
  }
};

const removeRemoteTag = (tagName) => {
  const tagExists = remoteTagExists(tagName);

  if (tagExists) {
    const executable = 'git';
    const args = ['push', 'origin', '--delete', tagName];

    try {
      const result = execa.sync(executable, args, { stdio: 'pipe' });

      if (result.exitCode !== 0) {
        throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
      }
    } catch (e) {
      console.error(e);

      process.exit(1);
    }
  }
};

const removeLocalBranch = (branchName) => {
  const localExists = localBranchExists(branchName);

  if (localExists) {
    const executable = 'git';
    const args = ['branch', '-D', branchName];

    try {
      const result = execa.sync(executable, args, { stdio: 'pipe' });

      if (result.exitCode !== 0) {
        throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
      }
    } catch (e) {
      console.error(e);

      process.exit(1);
    }
  }
};

const removeLocalTag = (tagName) => {
  const localExists = localTagExists(tagName);

  if (localExists) {
    const executable = 'git';
    const args = ['tag', '-d', tagName];

    try {
      const result = execa.sync(executable, args, { stdio: 'pipe' });

      if (result.exitCode !== 0) {
        throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
      }
    } catch (e) {
      console.error(e);

      process.exit(1);
    }
  }
};

const ensureCWDIsClean = () => {
  // Throws error when local branch nor remote doesn't exist.
  // Throws error for untracked files, changes to be commited, changes not staged for commit, or commits not pushed.
  const currentLocalBranch = getCurrentLocalBranch();
  const localExists = localBranchExists(currentLocalBranch);
  const remoteExists = remoteBranchExists(currentLocalBranch);

  if (!localExists || !remoteExists) {
    throw new Error(`The branch "${currentLocalBranch}" doesn't exists.`);
  }

  const tab = ' '.repeat(4);
  const newLine = '\r\n';
  const untrackedFiles = getUntrackedFiles();
  const changesToBeCommited = getChangesToBeCommited();
  const changesNotStagedForCommit = getChangesNotStagedForCommit();
  
  if (untrackedFiles.length) {
    throw new Error(`${newLine}${tab}Found some untracked files:${newLine}${untrackedFiles.map(x => `${tab}  - ${x}`).join(newLine)}`);
  }

  if (changesToBeCommited.length) {
    throw new Error(`${newLine}${tab}Found some changes to be commited:${newLine}${changesToBeCommited.map(x => `${tab}  - ${x}`).join(newLine)}`);
  }

  if (changesNotStagedForCommit.length) {
    throw new Error(`${newLine}${tab}Found some files not staged for commit:${newLine}${changesNotStagedForCommit.map(x => `${tab}  - ${x}`).join(newLine)}`);
  }

  const commitHash = getLastCommitHash();

  if (commitHash.local !== commitHash.remote) {
    throw new Error(`Please push the commit "${commitHash.local}" to continue.`);
  }
};

const checkout = (branchName) => {
  const executable = 'git';
  
  try {
    const args = ['checkout', branchName];
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const createBranch = (branchName) => {
  const executable = 'git';
  
  try {
    const args = ['checkout', '-b', branchName];
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const createAndPushTag = (tagName) => {
  const executable = 'git';

  try {
    const args = ['tag', tagName];
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }

  try {
    const args = ['push', 'origin', tagName];
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const commit = (message) => {
  const executable = 'git';

  try {
    const args = ['add', '.'];
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }

  try {
    const args = ['commit', '-m', `${message}`];

    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const push = () => {
  const executable = 'git';

  try {
    const args = ['push', 'origin', getCurrentLocalBranch()];
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
};

const fetch = (tagOrBranchName) => {
  const executable = 'git';

  try {
    const args = ['fetch', 'origin', tagOrBranchName];
    const result = execa.sync(executable, args, { stdio: 'pipe' });

    if (result.exitCode !== 0) {
      throw new Error(`Command ${executable} ${args.join(' ')} exited with status code ${result.exitCode}.`);
    }
  } catch (e) {
    console.error(e);

    process.exit(1);
  }
}

module.exports = {
  getRemoteOriginUrl,
  getCurrentLocalBranch,
  getLocalLastCommitHash,
  getRemoteLastCommitHash,
  getLastCommitHash,
  getUntrackedFiles,
  getChangesToBeCommited,
  getChangesNotStagedForCommit,
  remoteBranchExists,
  remoteTagExists,
  localBranchExists,
  localTagExists,
  removeLocalBranch,
  removeLocalTag,
  removeRemoteBranch,
  removeRemoteTag,
  ensureCWDIsClean,
  checkout,
  createBranch,
  createAndPushTag,
  commit,
  push,
  fetch,
};
