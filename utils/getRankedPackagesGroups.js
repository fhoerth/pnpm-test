/**
 * When building packages for first time (or cleaning dist directories),
 * the internal dependencies (@internal-portals/*) aren't built and
 * and the internal packages that depend on other internal packages
 * will throw errors.
 * This script executes commands by ordering internal packages by "ranking".
 * The ranking of a package will depend on the internal packages dependencies.
 * For example: if package "a" requires package "b", and package "b" requires package "c",
 * then the ranking for each package would be:
 * c: 1 (because c doesn't have any internal dependency)
 * b: 2 (because it depends on a level 1 dependency)
 * a: 3 (because it depends on a level 2 dependency)
 * This way, the command will be executed by levels, preventing the errors mentioned before.
 * Each ranking could have more than one package, and in that case the `command` is executed
 * simultaneously for that ranking.
 */
const chalk = require('chalk');
const process = require('process');

const getPackages = require('./getPackages');
const findPackageByName = require('./findPackageByName');
const getInternalDependencies = require('./getInternalDependencies');

const packages = getPackages();

const getPackageRanking = (packageName, _trace = []) => {
  const pkg = findPackageByName(packageName);
  const internalDependencies = getInternalDependencies(pkg['package.json']);

  const internalDependenciesList = [
    ...internalDependencies.dependencies,
    ...internalDependencies.devDependencies,
    ...internalDependencies.peerDependencies,
  ];

  if (_trace.includes(packageName)) {
    const stringifiedTrace = _trace
      .concat(packageName)
      .join(' -> ')
      .replace(new RegExp(packageName, 'g'), chalk.bold(packageName));
    process.stdout.write(
      chalk.red(`Circular dependency encountered:\n ${stringifiedTrace}\n`)
    );
    process.exit(1);
  }

  const trace = _trace.concat(packageName);

  const dependenciesRanking = internalDependenciesList
    .map(package => getPackageRanking(package.name, trace))
    .reduce((acc, ranking) => acc + ranking, 0);
  
  return dependenciesRanking + 1;
};

function getRankedPackagesGroups() {
  const rankedSortedPackages = packages
    .map(x => ({
      ...x,
      ranking: getPackageRanking(x.name)
    }))
    .sort((a, b) => a.ranking >= b.ranking);

  const rankedPackagesGroups = rankedSortedPackages
    .reduce((acc, pkg) => {
      const packageIdx = pkg.ranking - 1;

      if (typeof acc[packageIdx] === 'undefined') {
        acc[packageIdx] = [];
      }

      acc[packageIdx].push(pkg);

      return acc;
    }, [])
    .filter(x => x);
  
  return rankedPackagesGroups;
}

module.exports = getRankedPackagesGroups;
