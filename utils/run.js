const path = require('path');
const pty = require('node-pty');
const EventEmitter = require('events');

const Buffer = require('./Buffer');
const createColors = require('./colors');
const getFilteredRankedPackagesGroups = require('./getFilteredRankedPackagesGroups');

const parallelIdx = process.argv.indexOf('--parallel');
const streamIdx = process.argv.indexOf('--stream');

const dirIdx = process.argv.indexOf('--dir');
const cmdIdx = process.argv.indexOf('--cmd');
const dir = dirIdx !== -1 ? process.argv[dirIdx + 1] : null;
const [executable, ...processArgs] = process.argv.slice(cmdIdx + 1, process.argv.length);

const streaming = !!process.argv[streamIdx];
const isParallel = streaming || !!process.argv[parallelIdx];

const rankedPackagesGroups = getFilteredRankedPackagesGroups(dir);
const colors = createColors();

let currentPackageName = null;

function runGroup(group) {
  const groupImmediate = new EventEmitter();

  let packagesFinished = 0;

  groupImmediate.on('next', () => {
    const nextGroup = rankedPackagesGroups.shift();

    if (nextGroup) {
      runGroup(nextGroup);
    }
  });

  groupImmediate.on('groupPackageFinished', () => {
    packagesFinished += 1;

    if (packagesFinished === group.length) {
      groupImmediate.emit('next');
    }
  });

  if (isParallel) {
    groupImmediate.emit('next');
  }

  group.forEach(package => {
    const color = colors.next();
    const terminal = pty.spawn(
      executable,
      processArgs,
      {
        name: 'xterm-color',
        cwd: path.join(process.cwd(), package.relativePath),
        cols: process.stdout.columns - 2,
        rows: process.stdout.rows,
        env: process.env,
      },
    );

    const buffer = new Buffer({
      packageName: package.name,
      executable,
      processArgs,
      color,
      streaming,
    });

    terminal.on('data', (data) => {
      if (streaming) {
        if (currentPackageName !== package.name) {
          buffer.clear();
          buffer.printHeader();
        }
  
        buffer.concat(data);
  
        currentPackageName = package.name;
      } else {
        buffer.concat(data);
      }
    });

    terminal.on('exit', (code) => {
      if (!streaming) {
        // https://nodejs.org/api/process.html#process_exit_codes
        const hadError = code !== 0;

        buffer.printHeader();
        buffer.flush();
        buffer.end(hadError, code);

        if (hadError) {
          process.exit(code);
        } else {
          if (!isParallel) {
            groupImmediate.emit('groupPackageFinished');
          }
        }
      }
    });
  });
}

runGroup(rankedPackagesGroups.shift());
